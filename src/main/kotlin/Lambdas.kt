fun main(){
    print(1)
}

fun containsEven(collection: Collection<Int>): Boolean =
    collection.any {it % 2 == 0}